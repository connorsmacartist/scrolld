;(function($) {

   $.scrolld = function(el, options) {

	   var defaults = {
		   data : Array(),
		   limit : 10,
		   page: 1,
		   offset: 0,
		   buffer: 200,
		   pageParameter: "page",
		   method: "get",
		   onPageChange: function(n) {}
	   }

	   var _scrll = this;
	   var __elm = el;
	   var __dataobj;
	  
	   var error;	
	   var webservice;	
	   var pg;
	   var pg_count;
	   var next_pg;
	   var prev_pg;
	   var staticDataArray;
	   var dataPage;
	   var dataMethod;
	   var isAjax;
	   var isLoading;
	   var offsetTop;
	   var limit;
	   var start;
	   var tmplate;
	   var buffr;
	   var scroll_pos;
	   var uri;
	   var pageParameter;
	   var pageLimitParameter;
	  
	   _scrll.settings = {}
  



	   var doScroll = function(){ 
		   console.log('doScroll');

		   getCurrentPage();

		   if(!isLoading){	
			  
			   commitLoad( findPageToLoad() );
		  
		   }
	   }
	  
	  
	   var commitLoad = function(o){
		   if(o && o.page>=1){
			   if(!isAjax){
				   o.data = pgDataFromArray(o);
				   renderPage(o);
			   } else {
				   pgDataFromWeb(o);
			   }	
		   }			
	   }
	  
	  
	   var findPageToLoad = function(){	
		  
		   if($(".scrolld-item" ).length<=0){
		  
			   /// no items, init
			   return {"page":pg, "dir":1};
	  
		   } else {
		  
			   var e, p;
			   scroll_pos = $(window).scrollTop();
			   if (scroll_pos >= (0.9*($(document).height() - $(window).height())) - buffr) {
				   e = __elm.find(">:last-child");
				   p = parseFloat( $(e).attr("data-page") ) + 1;
				   return {"page":p, "dir":1};
			   }					
			   if (scroll_pos <= ((0.9 * offsetTop) +buffr) ) {
				   e = __elm.find(">:first-child");
				   p = parseFloat( $(e).attr("data-page") ) - 1;	
				   return {"page":p, "dir":-1};
			   }
			   return false;
		  
		   }
	   }
					  
	  
	  
	   var renderPage = function(o){	
		   if(o.data && o.data.length>0){
			   if(o.dir<0){
				   o.data = o.data.reverse();
			   }
			   var offst = $(__elm).outerHeight();
			   for(var i=0; i<o.data.length; ++i){
				   o.data[i].page = o.page;
				   var elm = $('<div></div>').html(makeElm(o.data[i])).attr({"data-page":o.page}).addClass("scrolld-item");
				   if(o.dir>0){
					   __elm.append(elm);
				   } else {
					   __elm.prepend(elm);
				   }
			   }
			   if(o.dir<0){
				   resetTop(offst);
			   }
			   optimizeLoad();
		   }
	   }					
					  
	   var pgDataFromArray = function(o) {
		   start =  ((o.page*limit)-limit);
		   return staticDataArray.slice(start, (start + limit));
	   }				
	  
  
	   /// determine current page from elements on the page
	   var getCurrentPage = function(){
		   $( ".scrolld-item" ).each(function( index ) {
			   var itmP = parseFloat( $(this).attr("data-page") );
			   if(isVisible(this)){
				   pg = itmP;
				   return false;
			   }
		   });
		   if(pg<=0 || !pg){
			   pg = 1;
		   }					
		   updateURI();
	   }
	  
	  
	  
	   /// determine current page from elements on the page
	   var getCurrentPageFromUri = function(){
		   var p = 1;
		   var regex = new RegExp("[\\?&]" + pageParameter + "=([^&#]*)");
		   var results = regex.exec(document.URL);
		   if(results){
			   p = (results[1]);
		   }
		   return p;
	   }				
	  

	   /// history.replaceState for SEO
	   var updateURI = function() {
		   if(!uri){
			   uri = document.createElement('a');
		   }
		   uri.href =  document.URL;	
		   if(history.replaceState){	
			   if(pageParameter){
				   if(uri.search.indexOf(pageParameter+"=") > -1){
					   var regex = new RegExp("[\\?&]" + pageParameter + "=([^&#]*)");
					   var results = regex.exec(document.URL);
					   history.replaceState(null, null,  document.URL.replace(pageParameter+'='+results[1], pageParameter+'='+pg) );	
				   } else {
					   history.replaceState(null, null,  document.URL + ((uri.search) ? '&' : '?') + pageParameter+'='+pg );			
				   }		
			   }
		   }
	   }
	  

	   /// remove/add items to preserve memory
	   var optimizeLoad = function(){	
		   var offst = 0;
		   var optmiz = false;
		   $( ".scrolld-item" ).each(function( index ) {
			   var itmP = parseFloat( $(this).attr("data-page") );
			   if(itmP<(pg-2) || itmP>(pg+2)){
				   if(itmP<(pg-2) && !optmiz){
					   offst = $(__elm).outerHeight();
					   optmiz = true;
				   }
				   if(itmP>(pg+2)){
					   if(optmiz){
						   resetTop(offst);
						   optmiz = false;
					   }
					   offst = 0;
				   }					  		
				   if(! isVisible(this)){
					   $(this).remove();
				   }
			   }
		   });	
		   if(optmiz && offst>0){
			   resetTop(offst);
			   optmiz = false;
		   }
		  
		   checkScrollPage();									
	   }				
	  
	  
	   /// check the page is scrollable
	   var checkScrollPage = function(){	
		   if(!error){
			   if(pg_count && pg < pg_count && (__elm.height() < $(window).height())){
				   /// data doesn't go to bottom of the page, add more pages
				   doScroll();
				   return;
			   }
			  
			   if(pg>1 && $(window).scrollTop()==0){
				   /// add the previous page
				   o = new Object();
				   o.page = (pg-1);
				   o.dir = -1; 
				   commitLoad(o);
				   return;
			   }
		   }
	   }				
	  
	  
	   /// move the scroll top when elements are added/removed
	   var resetTop = function(offst){
		   $(window).scrollTop(  $(window).scrollTop() + ($(__elm).outerHeight() - offst) );
	   }
	  
	   /// check if element is visible in viewport
	   var isVisible = function(elm){
		   var $elem = $(elm);
		   var $window = $(window);
		   var docViewTop = $window.scrollTop();
		   var docViewBottom = docViewTop + $window.height();
		   var elemTop = $elem.offset().top;
		   var elemBottom = elemTop + $elem.height();
		   return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));					
	   }
	  


	   /// create element, use mustache or just innerhtml
	   var makeElm = function(dt){		
		   if(Mustache && template){
			   return Mustache.to_html(template, dt);
		   }	
		   return $('<span></span>').html(dt);
	   }

									  





	   var pgDataFromWeb = function(o) {
	  
		   isLoading = true;

		   __dataobj[pageParameter] = pg;
		  
		   var request = $.ajax({
			   url: webservice,
			   method: dataMethod,
			   data: __dataobj,
			   dataType: "json"
		   }).done(function( json ) {
			   isLoading = false;
			   if(json.page_count){
				   pg_count = json.page_count;
			   }
			   o.data = json.data;
			   renderPage(o);
		   });
		  
	   }	






	   var init = function() {
	  
		   _scrll.settings = $.extend({}, defaults, options);
		   _scrll.el = el;
	  
		   /// settings
		   limit = _scrll.settings.limit;
		   pg = _scrll.settings.page;
		   offsetTop = _scrll.settings.offset;
		   buffr = _scrll.settings.buffer;
		   pageParameter = _scrll.settings.pageParameter; 
		   pageCountParameter = _scrll.settings.pageCountParameter; 
		   pageLimitParameter = _scrll.settings.pageLimitParameter; 
		   dataMethod = _scrll.settings.method; 
		  
		   if(pageParameter){
			   pg = getCurrentPageFromUri();
		   }					
		  
		  
		   /// templates
		   if(_scrll.settings.mustache){
			   template = $('#'+_scrll.settings.mustache).html();
		   }									
		  
		   /// static data or web service
		   staticDataArray = Array();
		   pg_count = 1;
		   isAjax = false;
		   if( typeof _scrll.settings.data === 'string' ) {
			   isAjax = true;
			   webservice  = _scrll.settings.data;
			   __dataobj = new Object();
		   } else {
			   staticDataArray = _scrll.settings.data;
			   pg_count = Math.ceil(staticDataArray.length / limit);
		   }
			  
		   /// bind events
		   $(window).scroll(function() {		
			   doScroll();
		   });

		   $(window).resize(function() {
			   doScroll();
		   });				
		  
		   /// start
		   doScroll();
	   }
  
  
	   init();
			  
									  
					  
	   /// public methods
	   _scrll.gotoPage = function(pg) {
	   }

  
   }

})(jQuery);	